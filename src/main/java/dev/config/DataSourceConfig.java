package dev.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
@Configuration
public class DataSourceConfig {

	@Bean
	public DataSource dataSource(
			@Value("${bdd.driver}") String driver,
			@Value("${bdd.user}") String user,
			@Value("${bdd.pass}") String pass,
			@Value("${bdd.url}") String url
			) {
		
		DriverManagerDataSource dataSource=new DriverManagerDataSource();
		dataSource.setDriverClassName(driver);
		dataSource.setUsername(user);
		dataSource.setPassword(pass);
		dataSource.setUrl(url);
		
		return dataSource;
	}
	
	
	
	
}
