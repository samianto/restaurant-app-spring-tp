package dev.dao;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dev.config.jdbcTestConfig;
import dev.entite.Plat;
import dev.service.PlatServiceVersion2;

@ActiveProfiles("jdbc")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {jdbcTestConfig.class, PlatServiceVersion2.class, PlatDaoJdbc.class }) // conf context spring

class PlatDaoJdbcIntegrationTest {

	@Autowired
	private PlatServiceVersion2 psv2;

	@Test
	void listerPlatsNonVide() {
		List<Plat> plats=psv2.listerPlats();
		Assertions.assertThat(plats.size()).isNotEqualTo(0);
	}
	
	@Test
	void ajouterPlat() {
		psv2.ajouterPlat("azerty", 5000);
		List<Plat> plats=psv2.listerPlats();
		Assertions.assertThat(plats).extracting(Plat::getNom).contains("azerty");
		Assertions.assertThat(plats).extracting(Plat::getPrixEnCentimesEuros).contains(5000);	
	}

}
