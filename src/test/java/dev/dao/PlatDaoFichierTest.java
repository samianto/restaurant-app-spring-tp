package dev.dao;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import dev.config.AppConfig;
import dev.entite.Plat;
import dev.ihm.Menu;
import dev.service.PlatServiceVersion2;

@TestPropertySource("classpath:test.properties")
class PlatDaoFichierTest {
	private AnnotationConfigApplicationContext context;
	private PlatServiceVersion2 psv2;

	@BeforeEach
	public void start() {
		context = new AnnotationConfigApplicationContext(PlatServiceVersion2.class, PlatDaoMemoire.class);
		psv2 = context.getBean(PlatServiceVersion2.class);
	}

	@AfterEach
	public void close() {
		context.close();
	}

	@Test
	public void TestAjoutPlat() {
		psv2.ajouterPlat("azerty", 10000);
		List<Plat> list = psv2.listerPlats();
		Assertions.assertThat(list.size()).isEqualTo(1);
	}

	@Test
	public void TestAjoutPlatBis() {
		psv2.ajouterPlat("azerty", 10000);
		List<Plat> list = psv2.listerPlats();
		Assertions.assertThat(list.size()).isEqualTo(1);
	}

}
