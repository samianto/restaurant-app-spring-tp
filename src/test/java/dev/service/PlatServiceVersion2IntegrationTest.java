package dev.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dev.dao.PlatDaoMemoire;
import dev.entite.Plat;
import dev.exception.PlatException;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { PlatServiceVersion2.class, PlatDaoMemoire.class }) // conf context spring
class PlatServiceVersion2IntegrationTest {

	@Autowired
	private PlatServiceVersion2 psv2;

	@Test
	public void testIntegrateAjoutPlat() {

		psv2.ajouterPlat("azerty", 6000);
		List<Plat> list = psv2.listerPlats();
		Assertions.assertThat(list.size()).isEqualTo(1);
	}

	@Test
	public void testIntegratePrixInvalid() {
		try {
			psv2.ajouterPlat("azerty", 6);
			fail("doit produire une exception");
		} catch (PlatException e) {
			Assertions.assertThat(e.getMessage()).isEqualTo("le prix d'un plat doit être supérieur à 10 €");
		}
	}
}
